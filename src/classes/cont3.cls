public class cont3{
    
    public list<UserInfo__c> us;
    public String un{get;set;}
    public String pw{get;set;}   
    public String name{get;set;}
    public String email{get;set;}
    public String phone{get;set;}
    public String id;
    public Boolean lockval{get;set;}
    public Boolean lockbtn{get;set;}
     public Boolean enableSave{get;set;}
    public String pageid;
    public cont3()
    {
        lockval=true;
        lockbtn=true;
        enableSave=false;
    }
    
    public PageReference login() {
        PageReference newPage;
        
        us=[select Id,user__c,name__c,phone__c,email__c from UserInfo__c where user__c=:un AND password__c=:pw];
        
        if(us.size()>0)
        { 
            for(UserInfo__c uin:us){
                id=uin.id;
            }
            newPage = new PageReference('https://c.ap4.visual.force.com/apex/AfterLogin?id='+id);
            newPage.getParameters().put('id',us[0].id);
            newPage.setRedirect(true);
        }
        else
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'uaername or password is incorrect'));
        }
        return newPage; 
    }
    
    /*  public PageReference login()
{
PageReference  a1=new PageReference('https://c.ap4.visual.force.com/apex/AfterLogin?core.apexpages.request.devconsole=1');
a1.setRedirect(true);
return a1;     
}*/
    public PageReference cancel()
    {
        PageReference  a2;
        a2=new PageReference('https://c.ap4.visual.force.com/apex/login?core.apexpages.request.devconsole=1');
        a2.setRedirect(true);
        return a2;      
    }
    public PageReference logout()
    {
        PageReference  a2=new PageReference('https://c.ap4.visual.force.com/apex/login?core.apexpages.request.devconsole=1');
        a2.setRedirect(true);
        return a2;      
    }
    public PageReference save() {
        PageReference newPage3 = new PageReference('https://c.ap4.visual.force.com/apex/AfterLogin?core.apexpages.request.devconsole=1');
        newPage3.getParameters().put('id',us[0].id);
        newPage3.setRedirect(true);
        update us;
        return newPage3;} 
    
    public void edit() {
        pageid=ApexPages.currentPage().getParameters().get('id');
        List<UserInfo__c> getrecs=[select user__c,email__c,phone__c,name__c from UserInfo__C where id =: pageid];
        for(UserInfo__c uic:getrecs)
        {
            un=uic.user__c;
            email=uic.email__c;
            phone=uic.phone__c;
            name=uic.name__c;      
        }
        /*PageReference newPage1 = new PageReference('https://c.ap4.visual.force.com/apex/Signup?core.apexpages.request.devconsole=1'); 
newPage1.getParameters().put('id',us[0].id);
newPage1.setRedirect(true);
return newPage1;*/
    }
    /*public PageReference editPage() {
PageReference newPage1 = new PageReference('https://c.ap4.visual.force.com/apex/editpage?core.apexpages.request.devconsole=1&id='+us[0].id); 
//ApexPages.addMessage(ApexPages.message(ApexPages.Severity.INFO,'my id'+b.us[0].id));
System.debug(us[0].id);
// newPage1.getParameters().put('id',b.us[0].id);
newPage1.setRedirect(true);
return newPage1;
}*/
    public void enableTextEdit()
    {
        
        lockval=false;
        lockbtn=false;
        enableSave=true;
    }
    public void saveMyData()
    {
        lockval=true;
        lockbtn=true;
        enableSave=false;
       UserInfo__c updateList=[select user__c,email__c,phone__c,name__c from UserInfo__C where id =: pageid];
        
        updateList.name__c=name;
       updateList.user__c=un;
       updateList.email__c=email;
       updateList.phone__c=phone; 
           
        update updateList;
        
    }
    
    
}