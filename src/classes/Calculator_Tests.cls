@isTest
private class Calculator_Tests {
    @isTest static void testAddition() {
        Calculator.addition(1, 1);
        System.assertEquals(Calculator.addition(1, 1), 2, 'Expected to get 2');
    }
    @isTest static void testSubtraction() {
        Calculator.subtraction(1, 1);
        System.assertEquals(Calculator.subtraction(1, 1), 0, 'Expected to get 0');
    }
    @isTest static void testMultiply() {
        Calculator.multiply(1, 1);
        System.assertEquals(Calculator.multiply(1, 1), 1, 'Expected to get 1');
    }
    @isTest static void testMultiplyWithZero() {
        List<Boolean> exceptions = new List<Boolean>();
		
		  try{
            Calculator.multiply(1, 0);
        } catch (Calculator.CalculatorException awe){
            if(awe.getMessage().equalsIgnoreCase('It doesn\'t make sense to multiply by zero')){
                exceptions.add(true);
            }
        }
        for(Boolean b : exceptions){
            system.assert(b, 'It doesn\'t make sense to multiply by zero');
        }
    }
    
    @isTest static void testDivide() {
        Calculator.divide(1, 1);
        System.assertEquals(Calculator.divide(1, 1), 1, 'Expected to get 1');
    }
    @isTest static void testDivideWithZero() {
        List<Boolean> exceptions = new List<Boolean>();
		
		 try{
            Calculator.divide(1, 0);
        } catch (Calculator.CalculatorException awe){
            if(awe.getMessage().equalsIgnoreCase('you still can\'t divide by zero')){
                exceptions.add(true);
            }
        }
        for(Boolean b : exceptions){
            system.assert(b, 'you still can\'t divide by zero');
        }
    }
    @isTest static void testDivideWithNegative() {
        List<Boolean> exceptions = new List<Boolean>();
		
		 try{
            Calculator.divide(-1, 1);
        } catch (Calculator.CalculatorException awe){
            if(awe.getMessage().equalsIgnoreCase('Division returned a negative value.')){
                exceptions.add(true);
            }
        }
        for(Boolean b : exceptions){
            system.assert(b, 'Division returned a negative value.');
        }
    }
}