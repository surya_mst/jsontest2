/*public class cont2 {

}
apex:page controller = "testController">
   <apex:form>
     <apex:pageBlock>
       <apex:inputText value= "{!sometext}" />
     </apex:pageBlock>
    </apex:form>
<apex:page>*/

public class cont2 {
   public String uname{get;set;}
    public String pwd{get;set;}
    public String rpwd{get;set;}
    public String eid{get;set;}
    public String ph{get;set;}
    public String name{get;set;}
    	PageReference a2;
    List<UserInfo__c> userdata=new List<UserInfo__c>();
   public UserInfo__c a1{get;set;}
   public cont2 () {
       a1=new UserInfo__c();
   } 
   public PageReference toSave() {
       list<UserInfo__c> us=[select id,user__c,password__c from UserInfo__c where user__c=:uname AND password__c=:pwd];
       if(uname==null || pwd==null || rpwd==null || eid==null || ph==null || name==null)
       {
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Fill all the fields')); 
       }
		 else if(us.size()>0)
        {
         
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'The username already exist.')); 
        }
       else if(pwd==rpwd)
       {
            a1.user__c = uname;
       a1.password__c = pwd;
           a1.re_pwd__c = rpwd;
       a1.email__c = eid;
           a1.phone__c=ph;
           a1.name__c=name;
      userdata.add(a1);
       insert a1;
          a2=new PageReference('https://c.ap4.visual.force.com/apex/login?core.apexpages.request.devconsole=1');
        a2.setRedirect(true);
          
       }
       else 
       {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'passward and retype password should be same'));
       }
    	  
    return a2;
   }
     public PageReference toCancel()
       {
   		PageReference a4=new PageReference('https://c.ap4.visual.force.com/apex/Signup?core.apexpages.request.devconsole=1');
        a4.setRedirect(true);
        return a4;
     }
   
	
}